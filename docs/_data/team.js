export const core = [
  {
    avatar: '/images/staff/hungnn.jpg',
    name: 'Nguyễn Hữu Hùng',
    title: 'Tổng giám đốc công ty IRTech',
    desc: 'Mục tiêu của chúng tôi là trở thành công ty công nghệ và Startup hàng đầu Việt Nam thông qua những công nghệ hiện đại, có tính đột phá và hướng đến giá trị chia sẻ cộng đồng.',
  },
  {
    avatar: '/images/staff/binhtnt.jpg',
    name: 'Tạ Ngọc Thiên Bình',
    title: 'Project Manager',
    desc: 'Chúng tôi mang đến sản phẩm công nghệ chất lượng nhằm giúp cho các doanh nghiệp thay đổi phương thức kinh doanh thông qua tự động hóa và tối ưu hóa hoạt động, tạo dựng cơ hội phát triển cho khách hàng, nâng cao hiệu suất vận hành và phát triển tương lai.',
  },
  {
    avatar: '/images/staff/baohv.png',
    name: 'Hoàng Bảo',
    title: 'Technical Leader',
    desc: 'Hy vọng sản phẩm của chúng tôi giúp ích cho doanh nghiệp của bạn',
  },
]

export const emeriti = [
  {
    avatar: '/images/staff/hangntd.jpg',
    name: 'Diệu Hằng',
    title: 'Sale',
    desc: 'Email: hangntd@irtech.com.vn',
    links: [{ icon: 'facebook', link: 'https://www.facebook.com/IrtechVietNam' }],
  },
  {
    avatar: '/images/staff/hoamy.jpg',
    name: 'Họa My',
    title: 'Marketing',
    desc: 'Email: mylth@irtech.com.vn',
    links: [{ icon: 'facebook', link: 'https://www.facebook.com/IrtechVietNam' }],
  },
]
