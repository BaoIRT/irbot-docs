---
layout: home

title: IRBot
titleTemplate: Công nghệ RPA tự động hóa quy trình

hero:
  name: IRBot
  text: Thay đổi cách thức làm việc của bạn với RPA!
  tagline: Tự động hóa quy trình có tính lặp đi lặp lại. Nhanh hơn, chính xác hơn, tiết kiệm chi phí.
  image:
    src: /logo-with-shadow.png
    alt: IRBot
  actions:
    - theme: brand
      text: Xem hướng dẫn
      link: /guide/
    - theme: alt
      text: RPA là gì?
      link: /guide/what-is-rpa

features:
  - icon:
      src: /images/features/feature_1.svg
    title: Dễ sử dụng
    details: Các giao diện màn hình cách sử dụng đơn giản, người dùng dễ dàng thực hiện các thao tác và tự động hóa quy trình công nghệ RPA mà không cần phải có kiến thức chuyên sâu về lập trình.
  - icon:
      src: /images/features/feature_2.svg
    title: Tích hợp đa nền tảng
    details: IRBOT dễ dàng tự động hóa với tất cả ứng dụng ở trên Windows, tích hợp đơn giản với các phần mềm hay hệ thống có sẵn của doanh nghiệp như CRM, ERP, HRM,...
  - icon:
      src: /images/features/feature_3.svg
    title: Chi phí hợp lý
    details: IRBOT mang đến giải pháp tự động hóa thông minh với mức chi phí phù hợp theo nhu cầu sử dụng và quy mô doanh nghiệp.
  - icon:
      src: /images/features/feature_4.svg
    title: Đồng hành 24/7
    details: IRBOT hỗ trợ bạn 24/7 qua kênh liên lạc riêng, hỗ trợ các công cụ và tài nguyên bạn cần ngay khi cơ hội đến.
---
