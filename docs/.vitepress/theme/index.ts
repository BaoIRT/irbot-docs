import { h } from 'vue'
import Theme from 'vitepress/theme'
import './styles/vars.css'
import './styles/custom.css'
import HomeSponsors from './components/HomeSponsors.vue'
import SvgImage from './components/SvgImage.vue'

export default {
  ...Theme,
  Layout() {
    return h(Theme.Layout, null, {
      'home-features-after': () => h(HomeSponsors),
    })
  },
  enhanceApp({ app }) {
    app.component('SvgImage', SvgImage)
  },
}
