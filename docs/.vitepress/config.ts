import { DefaultTheme, defineConfig } from 'vitepress'

const ogDescription = 'Công nghệ RPA tự động hóa quy trình'
const ogImage = 'https://irbot.com.vn/og-image.png'
const ogTitle = 'Irbot'
const ogUrl = 'https://irbot.com.vn'

// netlify envs
export default defineConfig({
  title: `IRBot`,
  description: 'Công nghệ RPA tự động hóa quy trình',

  head: [
    ['link', { rel: 'icon', type: 'image/svg+xml', href: '/logo.svg' }],
    ['meta', { property: 'og:type', content: 'website' }],
    ['meta', { property: 'og:title', content: ogTitle }],
    ['meta', { property: 'og:image', content: ogImage }],
    ['meta', { property: 'og:url', content: ogUrl }],
    ['meta', { property: 'og:description', content: ogDescription }],
    ['meta', { name: 'twitter:card', content: 'summary_large_image' }],
    ['meta', { name: 'twitter:site', content: '@vite_js' }],
    ['meta', { name: 'theme-color', content: '#646cff' }],
    [
      'script',
      {
        src: 'https://cdn.usefathom.com/script.js',
        'data-site': 'CBDFBSLI',
        'data-spa': 'auto',
        defer: '',
      },
    ],
  ],

  locales: {
    root: { label: 'Việt Nam' }
  },

  themeConfig: {
    logo: '/logo.svg',

    editLink: {
      pattern: 'https://github.com/vitejs/vite/edit/main/docs/:path',
      text: 'Suggest changes to this page',
    },

    socialLinks: [
      { icon: 'facebook', link: 'https://www.facebook.com/IrtechVietNam' },
      { icon: 'linkedin', link: 'https://www.linkedin.com/company/irtech-viet-nam' }
    ],

    algolia: {
      appId: 'KL1NNQENDI',
      apiKey: '685f0b22a4c1c9274e4367cff006244a',
      indexName: 'irbot'
    },

    footer: {
      message: `A product of IRTech Company`,
      copyright: '© 2019-2023 IRTech. All rights reserved.',
    },


    nav: [
      { text: 'Hướng dẫn', link: '/guide/', activeMatch: '/guide/' },
      { text: 'Về chúng tôi', link: '/team', activeMatch: '/team/' },
      { text: 'IRBot Cloud', link: 'https://cloud.irbot.com.vn/' }
    ],

    sidebar: {
      '/guide/': [
        {
          text: 'Giới thiệu',
          items: [
            {
              text: 'RPA là gì?',
              link: '/guide/what-is-rpa',
            },
            {
              text: 'Giới thiệu về IRBot',
              link: '/guide/',
            }
          ],
        },
        {
          text: 'IRBot Cloud',
          collapsed: true,
          items: [
            {
              text: 'Tổng quan',
              link: '/guide/irbot-cloud/summary',
            },
            {
              text: 'Project',
              link: '/guide/irbot-cloud/project',
            },
            {
              text: 'Robot',
              link: '/guide/irbot-cloud/robot',
            },
            {
              text: 'Job',
              link: '/guide/irbot-cloud/job',
            },
            {
              text: 'Schedule',
              link: '/guide/irbot-cloud/schedule',
            },
            {
              text: 'API',
              link: '/guide/irbot-cloud/api',
            },
            {
              text: 'Event subscriptions',
              link: '/guide/irbot-cloud/event-subscription',
            },
          ],
        },
        {
          text: 'IRBot Studio',
          collapsed: true,
          items: [
            {
              text: 'Tải xuống & Cài đặt',
              link: '/guide/irbot-studio/setup',
            },
            {
              text: 'Hướng dẫn sử dụng',
              link: '/guide/irbot-studio/guideline',
            },
            {
              text: 'Activities',
              collapsed: true,
              items: [
                {
                  text: 'Programming',
                  collapsed: true,
                  items: [
                    {
                      text: 'Activity 1',
                      link: '/guide/irbot-studio/activities/programming/activity1',
                    },
                  ]
                },
                {
                  text: 'Core',
                  collapsed: true,
                  items: [
                    {
                      text: 'Activity 1',
                      link: '/guide/irbot-studio/activities/core/activity1',
                    },
                  ]
                },
                {
                  text: 'App Integration',
                  collapsed: true,
                  items: [
                    {
                      text: 'Activity 1',
                      link: '/guide/irbot-studio/activities/app-integration/activity1',
                    },
                  ]
                },
                {
                  text: 'Web Browser',
                  collapsed: true,
                  items: [
                    {
                      text: 'Activity 1',
                      link: '/guide/irbot-studio/activities/web-browser/activity1',
                    },
                  ]
                },
                {
                  text: 'Windows',
                  collapsed: true,
                  items: [
                    {
                      text: 'Activity 1',
                      link: '/guide/irbot-studio/activities/window/activity1',
                    },
                  ]
                },
              ]
            }
          ],
        },
        {
          text: 'IRBot Agent',
          collapsed: true,
          items: [
            {
              text: 'Tải xuống & Cài đặt',
              link: '/guide/irbot-agent/setup',
            },
            {
              text: 'Hướng dẫn sử dụng',
              link: '/guide/irbot-agent/guideline',
            }
          ],
        },
      ],
    },
  },
})
