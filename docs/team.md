---
layout: page
title: Đội ngũ IRBot
description: Đội ngũ IRBot.
---

<script setup>
import {
  VPTeamPage,
  VPTeamPageTitle,
  VPTeamPageSection,
  VPTeamMembers
} from 'vitepress/theme'
import { core, emeriti } from './_data/team'
</script>

<VPTeamPage>
  <VPTeamPageTitle>
    <template #title>Đội ngũ phát triển</template>
    <template #lead>
      IRTech là đơn vị tiên phong mang đến giải pháp RPA với chi phí hợp lí giúp tăng năng suất, giảm nhân lực cho doanh nghiệp của bạn.
    </template>
  </VPTeamPageTitle>
  <VPTeamMembers :members="core" />
  <VPTeamPageSection>
    <template #title>Đội ngũ chăm sóc khách hàng</template>
    <template #lead>
      Hãy để IRTECH tư vấn, hỗ trợ giải đáp mọi vấn đề doanh nghiệp bạn đang gặp phải.
    </template>
    <template #members>
      <VPTeamMembers  :members="emeriti" />
    </template>
  </VPTeamPageSection>
</VPTeamPage>
